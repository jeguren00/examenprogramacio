package examen_2021_2022_DAM;



public class Drassana {
	
	//1. Inicialitzar naus (part I)
	public static void inicialitzarNaus(Drassana_Dades drassana_MCRN) {
		
	}

	
	//1. Inicialitzar naus (part II)
	public static void inicialitzarPecesNaus(Drassana_Dades drassana_MCRN) {
		/*
		Peces_electroniques de tipus radar: 
		("Cyrano IV", 0, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, true, 120, "kW");
		("Cyrano IV", 1, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", false, true, 120, "kW");
		("Cyrano IV", 2, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		
		
		Peces_electroniques de tipus visorIR: 
		("SAT SCM2400 Super Cyclone", 0, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		("SAT SCM2400 Super Cyclone", 1, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, true, 5, "kW");
		("SAT SCM2400 Super Cyclone", 2, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		 
		 
		Peces mampara de tipus mampara5x5:
		("mampara de 5x5", 0, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		("mampara de 5x5", 1, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, true, 5, 5);
		("mampara de 5x5", 2, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		*/
	}
	

	//2. Inicialitzar drassana
	public static void inicialitzarPecesDrassana(Drassana_Dades drassana_MCRN) {
		/*
		Peces_electroniques de tipus radar: 
		("Cyrano IV", 3, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, true, 120, "kW");
		("Cyrano IV", 4, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", false, true, 120, "kW");
		("Cyrano IV", 5, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		("Cyrano IV", 6, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		("Cyrano IV", 7, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		
		
		Peces_electroniques de tipus radio: 
		("radio UHF/VHF", 0, "radio salto frecuencias UHF/VHF", "Thomson-CSF", true, false, 10, "kW");
		("radio UHF/VHF", 1, "radio salto frecuencias UHF/VHF", "Thomson-CSF", true, false, 10, "kW");
		
		
		Peces_electroniques de tipus visorIR: 
		("SAT SCM2400 Super Cyclone", 0, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		("SAT SCM2400 Super Cyclone", 1, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		("SAT SCM2400 Super Cyclone", 2, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		
		
		Peces mampara de tipus mampara5x5:
		("mampara de 5x5", 0, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		("mampara de 5x5", 1, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		*/
	}
	
	
	//10. Drassana: veure stock de peces
	// Veure el nº de peces de repost que hi ha en la drassana.
	// Fer servir un iterador per les claus del mapa.
	public static void veureStockPecesEnDrassana(Drassana_Dades drassana_MCRN) {

	}
	
}
