package examen_2021_2022_DAM;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Drassana_Dades {
	protected String nomDrassana = "MCRN Calisto";
	//millor tipus de llista per fer insercions i eliminacions, no per accedir
	private LinkedList<Nau_Dades>llistaNausEnDrassana;
	//tipus de mapa que ordena segons la clau
	private TreeMap<String,Integer> mapaStockPeces;
	private TreeMap<Integer,LinkedList<Peça_electronica_Dades>> mapaPecesElectronica;
	private TreeMap<Integer,LinkedList<Peça_mampara_Dades>> mapaPecesMampares;
	
	public Drassana_Dades() {
		this.llistaNausEnDrassana = new LinkedList<Nau_Dades>();
		this.mapaStockPeces = new TreeMap<String,Integer>();
		this.mapaPecesElectronica = new TreeMap<Integer,LinkedList<Peça_electronica_Dades>>();
		this.mapaPecesMampares = new TreeMap<Integer,LinkedList<Peça_mampara_Dades>>() ;
	}

	public LinkedList<Nau_Dades> getLlistaNausEnDrassana() {
		return llistaNausEnDrassana;
	}

	public void setLlistaNausEnDrassana(LinkedList<Nau_Dades> llistaNausEnDrassana) {
		this.llistaNausEnDrassana = llistaNausEnDrassana;
	}

	public TreeMap<String,Integer> getMapaStockPeces() {
		return mapaStockPeces;
	}

	public void setMapaStockPeces(TreeMap<String,Integer> mapaStockPeces) {
		this.mapaStockPeces = mapaStockPeces;
	}

	public TreeMap<Integer, LinkedList<Peça_electronica_Dades>> getMapaPecesElectronica() {
		return mapaPecesElectronica;
	}

	public void setMapaPecesElectronica(TreeMap<Integer, LinkedList<Peça_electronica_Dades>> mapaPecesElectronica) {
		this.mapaPecesElectronica = mapaPecesElectronica;
	}

	public TreeMap<Integer, LinkedList<Peça_mampara_Dades>> getMapaPecesMampares() {
		return mapaPecesMampares;
	}

	public void setMapaPecesMampares(TreeMap<Integer, LinkedList<Peça_mampara_Dades>> mapaPecesMampares) {
		this.mapaPecesMampares = mapaPecesMampares;
	}
	
	
	

}
