package examen_2021_2022_DAM;

public class Peça_mampara_Dades extends Peça_prototipus_Dades {
	private int peça_altura;
	private int peça_amplada;
	
	public Peça_mampara_Dades(String peça_ID, int peça_num_serie, String peça_nom, String fabricant_ID, boolean peça_reparable, boolean peça_trencada, int peça_altura, int peça_amplada) {
		super(peça_ID, peça_num_serie, peça_nom, fabricant_ID, peça_reparable, peça_trencada);
		// TODO Auto-generated constructor stub
		this.peça_altura = peça_altura;
		this.peça_amplada = peça_amplada;
	}
	
	@Override
	public String toString() {
		return 	"Peça_electronica_Dades [peça_ID=" + getPeça_ID() + ", peça_num_serie=" + getPeça_num_serie() + ", peça_nom="
		+ getPeça_nom() + ", fabricant_ID=" + getFabricant_ID() + ", peça_reparable=" + isPeça_reparable()
		+ ", peça_trencada=" + isPeça_trencada() + ", peça_altura=" + this.getPeça_altura() + ", peça_altura=" + this.getPeça_altura() + "]";
	}
	
	@Override
	public boolean esReparable() {
		// TODO Auto-generated method stub
		return false;
	}

	public int getPeça_altura() {
		return peça_altura;
	}

	public void setPeça_altura(int peça_altura) {
		this.peça_altura = peça_altura;
	}

	public int getPeça_amplada() {
		return peça_amplada;
	}

	public void setPeça_amplada(int peça_amplada) {
		this.peça_amplada = peça_amplada;
	}
	
	
}
