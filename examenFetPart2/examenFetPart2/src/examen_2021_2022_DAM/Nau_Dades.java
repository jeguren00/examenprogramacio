package examen_2021_2022_DAM;

import java.util.ArrayList;


public class Nau_Dades {
	private String nau_ID;
	private String nau_nom;
	private ArrayList<Peça_electronica_Dades> llistaPecesElectronica = new ArrayList<Peça_electronica_Dades>();
	private ArrayList<Peça_mampara_Dades> llistaPecesMampara = new ArrayList<Peça_mampara_Dades>();
	
	
	public Nau_Dades(String nau_ID, String nau_nom, ArrayList<Peça_electronica_Dades> llistaPecesElectronica, ArrayList<Peça_mampara_Dades> llistaPecesMampara) {
		this.nau_ID = nau_ID;
		this.nau_nom = nau_nom;
		this.llistaPecesElectronica = llistaPecesElectronica;
		this.llistaPecesMampara = llistaPecesMampara;
	}


	public Nau_Dades(String nau_ID, String nau_nom) {
		this.nau_ID = nau_ID;
		this.nau_nom = nau_nom;
	}


	public Nau_Dades() {
	}


	public String getNau_ID() {
		return nau_ID;
	}

	public void setNau_ID(String nau_ID) {
		this.nau_ID = nau_ID;
	}

	public String getNau_nom() {
		return nau_nom;
	}

	public void setNau_nom(String nau_nom) {
		this.nau_nom = nau_nom;
	}

	public ArrayList<Peça_electronica_Dades> getLlistaPecesElectronica() {
		return llistaPecesElectronica;
	}

	public void setLlistaPecesElectronica(ArrayList<Peça_electronica_Dades> llistaPecesElectronica) {
		this.llistaPecesElectronica = llistaPecesElectronica;
	}

	public ArrayList<Peça_mampara_Dades> getLlistaPecesMampara() {
		return llistaPecesMampara;
	}

	public void setLlistaPecesMampara(ArrayList<Peça_mampara_Dades> llistaPecesMampara) {
		this.llistaPecesMampara = llistaPecesMampara;
	}
	
	
	
	
	
}
