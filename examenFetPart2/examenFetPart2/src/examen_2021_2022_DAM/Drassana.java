package examen_2021_2022_DAM;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.stream.Collectors;

public class Drassana {
	
	//1. Inicialitzar naus (part I)
	public static void inicialitzarNaus(Drassana_Dades drassana_MCRN) {
		Nau_Dades Donnager = new Nau_Dades("mcrn 101", "Donnager");
		Nau_Dades Pella = new Nau_Dades("mcrn 202", "Pella");
		Nau_Dades Carcassonne = new Nau_Dades("mcrn 303", "Carcassonne");
		
		drassana_MCRN.getLlistaNausEnDrassana().add(Donnager);
		drassana_MCRN.getLlistaNausEnDrassana().add(Pella);
		drassana_MCRN.getLlistaNausEnDrassana().add(0, Carcassonne);
		
		for(Nau_Dades nauTmp: drassana_MCRN.getLlistaNausEnDrassana()) {
			System.out.println("Drassana " + Drassana_Dades.nomDrassana + ": AFEGIDA LA NAU " + nauTmp.getNau_nom() + " AMB ID " + nauTmp.getNau_ID() + ".");
		}
	}

	
	//1. Inicialitzar naus (part II)
	public static void inicialitzarPecesNaus(Drassana_Dades drassana_MCRN) {
		Peça_electronica_Dades radar_000 = new Peça_electronica_Dades("Cyrano IV", 0, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, true, 120, "kW");
		Peça_electronica_Dades radar_001 = new Peça_electronica_Dades("Cyrano IV", 1, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", false, true, 120, "kW");
		Peça_electronica_Dades radar_002 = new Peça_electronica_Dades("Cyrano IV", 2, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		
		ArrayList <Peça_electronica_Dades> llista_radars = new ArrayList<Peça_electronica_Dades>(Arrays.asList(radar_000, radar_001, radar_002));
		
		Peça_electronica_Dades visorIR_000 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 0, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		Peça_electronica_Dades visorIR_001 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 1, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, true, 5, "kW");
		Peça_electronica_Dades visorIR_002 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 2, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		
		ArrayList <Peça_electronica_Dades> llista_visorsIR = new ArrayList<Peça_electronica_Dades>(Arrays.asList(visorIR_000, visorIR_001, visorIR_002));
		
		Peça_mampara_Dades mampara5x5_000 = new Peça_mampara_Dades("mampara de 5x5", 0, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		Peça_mampara_Dades mampara5x5_001 = new Peça_mampara_Dades("mampara de 5x5", 1, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, true, 5, 5);
		Peça_mampara_Dades mampara5x5_002 = new Peça_mampara_Dades("mampara de 5x5", 2, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		
		ArrayList <Peça_mampara_Dades> llista_mampares = new ArrayList<Peça_mampara_Dades>(Arrays.asList(mampara5x5_000, mampara5x5_001, mampara5x5_002));
		
		// Assignem les peces a les naus:
		int i = 0;
		ListIterator<Nau_Dades> it_naus = drassana_MCRN.getLlistaNausEnDrassana().listIterator(drassana_MCRN.getLlistaNausEnDrassana().size());
		while (it_naus.hasPrevious()) {
			Nau_Dades nauTmp = it_naus.previous(); 
			
			Peça_electronica_Dades radarTmp = llista_radars.get(i);
			Peça_electronica_Dades visorIRTmp = llista_visorsIR.get(i);
			Peça_mampara_Dades mamparaTmp = llista_mampares.get(i);
			
			nauTmp.getLlistaPecesElectronica().add(radarTmp);
			nauTmp.getLlistaPecesElectronica().add(visorIRTmp);
			nauTmp.getLlistaPecesMampara().add(mamparaTmp);
			System.out.println("Nau " + nauTmp.getNau_nom() + " (ID " + nauTmp.getNau_ID() + "):");
			System.out.println("    AFEGIDES LES PECES ELECTRÓNIQUES:");
			System.out.println("        " + radarTmp.getPeça_nom() + " Nº SERIE " + radarTmp.getPeça_num_serie());
			System.out.println("        " + visorIRTmp.getPeça_nom() + " Nº SERIE " + visorIRTmp.getPeça_num_serie());
			System.out.println("    AFEGIDES LES PECES MAMPARES:");
			System.out.println("        " + mamparaTmp.getPeça_nom() + " Nº SERIE " + mamparaTmp.getPeça_num_serie());
			
			i++;
		}
	}
	

	//2. Inicialitzar drassana
	public static void inicialitzarPecesDrassana(Drassana_Dades drassana_MCRN) {
		Peça_electronica_Dades radar_003 = new Peça_electronica_Dades("Cyrano IV", 3, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, true, 120, "kW");
		Peça_electronica_Dades radar_004 = new Peça_electronica_Dades("Cyrano IV", 4, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", false, true, 120, "kW");
		Peça_electronica_Dades radar_005 = new Peça_electronica_Dades("Cyrano IV", 5, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		Peça_electronica_Dades radar_006 = new Peça_electronica_Dades("Cyrano IV", 6, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		Peça_electronica_Dades radar_007 = new Peça_electronica_Dades("Cyrano IV", 7, "radar monopulso Thomson-CSF Cyrano IV", "Thomson-CSF", true, false, 120, "kW");
		
		drassana_MCRN.getMapaStockPeces().put("Cyrano IV", 5);
		drassana_MCRN.getMapaPecesElectronica().put("Cyrano IV", new LinkedList<Peça_electronica_Dades>(Arrays.asList(radar_003, radar_004, radar_005, radar_006, radar_007)));
		
		
		Peça_electronica_Dades radio_000 = new Peça_electronica_Dades("radio UHF/VHF", 0, "radio salto frecuencias UHF/VHF", "Thomson-CSF", true, false, 10, "kW");
		Peça_electronica_Dades radio_001 = new Peça_electronica_Dades("radio UHF/VHF", 1, "radio salto frecuencias UHF/VHF", "Thomson-CSF", true, false, 10, "kW");
		
		drassana_MCRN.getMapaStockPeces().put("radio UHF/VHF", 2);
		drassana_MCRN.getMapaPecesElectronica().put("radio UHF/VHF", new LinkedList<Peça_electronica_Dades>(Arrays.asList(radio_000, radio_001)));
		
		
		Peça_electronica_Dades visorIR_000 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 0, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		Peça_electronica_Dades visorIR_001 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 1, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		Peça_electronica_Dades visorIR_002 = new Peça_electronica_Dades("SAT SCM2400 Super Cyclone", 2, "cámara d'infrarojos SAT SCM2400 Super Cyclone", "Thomson-CSF", true, false, 5, "kW");
		
		drassana_MCRN.getMapaStockPeces().put("SAT SCM2400 Super Cyclone", 3);
		drassana_MCRN.getMapaPecesElectronica().put("SAT SCM2400 Super Cyclone", new LinkedList<Peça_electronica_Dades>(Arrays.asList(visorIR_000, visorIR_001, visorIR_002)));
		
		
		Peça_mampara_Dades mampara5x5_000 = new Peça_mampara_Dades("mampara de 5x5", 0, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		Peça_mampara_Dades mampara5x5_001 = new Peça_mampara_Dades("mampara de 5x5", 1, "mampara de nanopartícules de grafit de 5 x 5 metres", "Dassault", true, false, 5, 5);
		
		drassana_MCRN.getMapaStockPeces().put("mampara de 5x5", 2);
		drassana_MCRN.getMapaPecesMampares().put("mampara de 5x5", new LinkedList<Peça_mampara_Dades>(Arrays.asList(mampara5x5_000, mampara5x5_001)));
		
		// MOSTRAR LES QUANTITATS DE PECES PER PANTALLA --> SEL·LECCIONAR 1 --> MOSTRAR LES DADES DELS OBJECTES (DE LES PECES).
		System.out.println("Drassana " + Drassana_Dades.nomDrassana + ": ");
		System.out.println("    AFEGIDES LES PECES ELECTRÓNIQUES:");
		for (Entry<String, LinkedList<Peça_electronica_Dades>> entradaMapaTmp : drassana_MCRN.getMapaPecesElectronica().entrySet()) {
			for (Peça_electronica_Dades peçaTmp : entradaMapaTmp.getValue()) {
				System.out.println("        " + peçaTmp.getPeça_ID() + " Nº SERIE " + peçaTmp.getPeça_num_serie());	
			}
		}
		
		System.out.println("    AFEGIDES LES MAMPARES:");
		for (Entry<String, LinkedList<Peça_mampara_Dades>> entradaMapaTmp : drassana_MCRN.getMapaPecesMampares().entrySet()) {
			for (Peça_mampara_Dades peçaTmp : entradaMapaTmp.getValue()) {
				System.out.println("        " + peçaTmp.getPeça_ID() + " Nº SERIE " + peçaTmp.getPeça_num_serie());	
			}
		}
	}
	
	
	//10. Drassana: veure stock de peces
	// Veure el nº de peces de repost que hi ha en la drassana.
	// Fer servir un iterador per les claus del mapa.
	public static void veureStockPecesEnDrassana(Drassana_Dades drassana_MCRN) {
		String clau;
		
		Iterator<String> it = drassana_MCRN.getMapaStockPeces().keySet().iterator();
		while (it.hasNext()) {
			// it.next() agafa la clau i la fem servir per a buscar el valor en el mapa amb mapa.get(clau) .
			clau = it.next();
			System.out.println(clau + ": " + drassana_MCRN.getMapaStockPeces().get(clau));
		}
	}
	
	
	//11. Drassana: veure stock de peces ordenades pel nom (per la clau) sense tenir en compte majúscules, minúscules i accents.
	// Veure el nº de peces de repost que hi ha en la drassana ordenat per nom de peça (que resulta que és la clau del mapa).
	public static void veureStockPecesEnDrassanaOrdenatPerClau(Drassana_Dades drassana_MCRN) {		
		Collator collator = Collator.getInstance();
		collator.setStrength(Collator.TERTIARY);
		List<Entry<String,Integer>> mapaStockPeces = drassana_MCRN.getMapaStockPeces().entrySet().stream().sorted(Map.Entry.comparingByKey(collator)).collect(Collectors.toList());

		for (Entry<String, Integer> entradaMapaTmp : mapaStockPeces) {
			System.out.println(entradaMapaTmp.getKey() + ": " + entradaMapaTmp.getValue());
		}		

	}
	
	
	//12. Drassana: peces trencades de les naus
	// Ficar el resultat en un set ordenat per la data d'inserció i mostrar per pantalla el resultat (el que hi ha en la set).
	// Retornar el set perquè el faci servir el menú 13.
	public static HashSet pecesTrenacadesDeNausVSPecesRecanviDrassana(Drassana_Dades drassana_MCRN) {
		HashSet setPecesTrencades = new HashSet();
		
		for (Nau_Dades nau : drassana_MCRN.getLlistaNausEnDrassana()) {
			for (Peça_electronica_Dades pecaElectronica : nau.getLlistaPecesElectronica()) {
				if (pecaElectronica.isPeça_trencada()) {
					setPecesTrencades.add(pecaElectronica);
				}
			}
			
			for (Peça_mampara_Dades pecaElectronica : nau.getLlistaPecesMampara()) {
				if (pecaElectronica.isPeça_trencada()) {
					setPecesTrencades.add(pecaElectronica);
				}
			}
		}
		
		Iterator itr = setPecesTrencades.iterator();
		while(itr.hasNext()){
		  System.out.println(itr.next().toString());
		}

		return setPecesTrencades;
	}
	
	
	//13. Drassana: restar d'stock les peces trencades de les naus
	// A partir del resultat del menú 12 restar del stock de peces de la drassana les que es faran servir per a reparar les naus.
	public static void ferComandaPeces(Drassana_Dades drassana_MCRN, HashSet pecesTrencadesDeLesNaus) {
		String clau;
		for (Object nau : pecesTrencadesDeLesNaus) {
			if (nau instanceof Peça_electronica_Dades) {
				Peça_electronica_Dades objecteRecollit = (Peça_electronica_Dades) nau;
				for (Entry<String, Integer> peca : drassana_MCRN.getMapaStockPeces().entrySet()) {
					if (peca.getKey().equals(objecteRecollit.getPeça_ID())) {
						peca.setValue(peca.getValue() - 1);
					}
				}
			} else {
				Peça_mampara_Dades objecteRecollit = (Peça_mampara_Dades) nau;
				for (Entry<String, Integer> peca : drassana_MCRN.getMapaStockPeces().entrySet()) {
					if (peca.getKey().equals(objecteRecollit.getPeça_ID())) {
						peca.setValue(peca.getValue() - 1);
					}
				}

			}
		}
	}
	
	
	//14. Reparar naus
	// Implica recorrer les naus i per a cada nau treure la peça trencada (i esborrar-la i copiar-la en drassana si és reparable) i copiar-li una de la drassana (a llavors esborrar-li a la drassana aquesta peça).
	public static void repararRadarDeNaus(Drassana_Dades drassana_MCRN) {
		HashSet setPecesRadarDeDrassana = new HashSet();
		Nau_Dades nau;
		Iterator<Nau_Dades> it = drassana_MCRN.getLlistaNausEnDrassana().iterator();
		
		System.out.println("Drassana " + Drassana_Dades.nomDrassana + ": ");
		while (it.hasNext()) {
			nau = it.next();
			System.out.println("\t" + "PROCESANT LA NAU " + nau.getNau_nom() + " (" + nau.getNau_ID() + ")");
			for (Peça_electronica_Dades peca :nau.getLlistaPecesElectronica()) {
				System.out.println("\t" + "\t" + peca.toString());
				if(peca.getPeça_ID() == "Cyrano IV" && peca.isPeça_trencada() && peca.esReparable()) {
					setPecesRadarDeDrassana.add(peca);
				}
			}
		}
		
		System.out.println("Drassana " + Drassana_Dades.nomDrassana + ": ");
		System.out.println("\t" + "PECES ELECTRÓNIQUES DE TUPUS RADAR EMMAGATEMADES");
		Iterator<Peça_electronica_Dades> ite = setPecesRadarDeDrassana.iterator();
		while (ite.hasNext()) {
			System.out.println(ite.toString());
		}
	}

}
