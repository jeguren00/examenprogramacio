package examen_2021_2022_DAM;

public class Peça_electronica_Dades extends Peça_prototipus_Dades {
	private int peça_consumEnergetic;
	private String peça_consumEnergetic_Unitat_de_mesura;
	
	public Peça_electronica_Dades(String peça_ID, int peça_num_serie, String peça_nom, String fabricant_ID, boolean peça_reparable, boolean peça_trencada, int peça_consumEnergetic, String peça_consumEnergetic_Unitat_de_mesura) {
		super(peça_ID, peça_num_serie, peça_nom, fabricant_ID, peça_reparable, peça_trencada);
		// TODO Auto-generated constructor stub
		this.peça_consumEnergetic = peça_consumEnergetic;
		this.peça_consumEnergetic_Unitat_de_mesura = peça_consumEnergetic_Unitat_de_mesura;
	}


	@Override
	public String toString() {
		return 	"Peça_electronica_Dades [peça_ID=" + getPeça_ID() + ", peça_num_serie=" + getPeça_num_serie() + ", peça_nom="
		+ getPeça_nom() + ", fabricant_ID=" + getFabricant_ID() + ", peça_reparable=" + isPeça_reparable()
		+ ", peça_trencada=" + isPeça_trencada() + ", peça_consumEnergetic=" + this.getPeça_consumEnergetic() + ", peça_consumEnergetic_Unitat_de_mesura=" + this.getPeça_consumEnergetic_Unitat_de_mesura() + "]";
	}


	@Override
	public boolean esReparable() {
		// TODO Auto-generated method stub
		return false;
	}


	public int getPeça_consumEnergetic() {
		return peça_consumEnergetic;
	}


	public void setPeça_consumEnergetic(int peça_consumEnergetic) {
		this.peça_consumEnergetic = peça_consumEnergetic;
	}


	public String getPeça_consumEnergetic_Unitat_de_mesura() {
		return peça_consumEnergetic_Unitat_de_mesura;
	}


	public void setPeça_consumEnergetic_Unitat_de_mesura(String peça_consumEnergetic_Unitat_de_mesura) {
		this.peça_consumEnergetic_Unitat_de_mesura = peça_consumEnergetic_Unitat_de_mesura;
	}
	
}
