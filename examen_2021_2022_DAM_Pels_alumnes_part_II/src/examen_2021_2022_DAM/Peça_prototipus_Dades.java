package examen_2021_2022_DAM;

public abstract class Peça_prototipus_Dades {
	private String peça_ID;				// Identifica la peça (és com el DNI).
	private int peça_num_serie;			// Hi pot haver més d'una peça fabricada del mateix ID. Aquest nº les diferenciarà si tenen el mateix ID.
	private String peça_nom;
	private String fabricant_ID;
	private boolean peça_reparable;
	private boolean peça_trencada;
	

	public Peça_prototipus_Dades(String peça_ID, int peça_num_serie, String peça_nom, String fabricant_ID, boolean peça_reparable, boolean peça_trancada) {
		this.peça_ID = peça_ID;
		this.peça_num_serie = peça_num_serie;
		this.peça_nom = peça_nom;
		this.fabricant_ID = fabricant_ID;
		this.peça_reparable = peça_reparable;
		this.peça_trencada = peça_trancada;
	}
	
	
	abstract protected boolean esReparable();
	
}
